﻿CREATE TABLE [dbo].[Profesor]
(
	[Cedula] INT NOT NULL, 
    [Escuela] INT NULL,
	[Categoria] INT NULL,
	PRIMARY KEY ([Cedula]),
	CONSTRAINT [FK_dbo.Persona_dbo.Profesor] FOREIGN KEY ([Cedula]) REFERENCES [dbo].[Persona] ([Cedula])
)