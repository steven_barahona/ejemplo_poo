﻿/*
Deployment script for POO_DB

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "POO_DB"
:setvar DefaultFilePrefix "POO_DB"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Creating [dbo].[EstudiantePOO]...';


GO
CREATE TABLE [dbo].[EstudiantePOO] (
    [Cedula]  INT           NOT NULL,
    [Carnet]  NVARCHAR (15) NOT NULL,
    [Carrera] INT           NULL,
    [Nota]    INT           NULL,
    PRIMARY KEY CLUSTERED ([Cedula] ASC)
);


GO
PRINT N'Creating [dbo].[PersonaPOO]...';


GO
CREATE TABLE [dbo].[PersonaPOO] (
    [Cedula]    INT           NOT NULL,
    [Nombre]    NVARCHAR (50) NULL,
    [Apellido1] NVARCHAR (50) NULL,
    [Apellido2] NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Cedula] ASC)
);


GO
PRINT N'Creating [dbo].[ProfesorPOO]...';


GO
CREATE TABLE [dbo].[ProfesorPOO] (
    [Cedula]    INT NOT NULL,
    [Escuela]   INT NULL,
    [Categoria] INT NULL,
    PRIMARY KEY CLUSTERED ([Cedula] ASC)
);


GO
PRINT N'Creating [dbo].[FK_dbo.PersonaPOO_dbo.EstudiantePOO]...';


GO
ALTER TABLE [dbo].[EstudiantePOO] WITH NOCHECK
    ADD CONSTRAINT [FK_dbo.PersonaPOO_dbo.EstudiantePOO] FOREIGN KEY ([Cedula]) REFERENCES [dbo].[PersonaPOO] ([Cedula]);


GO
PRINT N'Creating [dbo].[FK_dbo.PersonaPOO_dbo.ProfesorPOO]...';


GO
ALTER TABLE [dbo].[ProfesorPOO] WITH NOCHECK
    ADD CONSTRAINT [FK_dbo.PersonaPOO_dbo.ProfesorPOO] FOREIGN KEY ([Cedula]) REFERENCES [dbo].[PersonaPOO] ([Cedula]);


GO
PRINT N'Checking existing data against newly created constraints';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[EstudiantePOO] WITH CHECK CHECK CONSTRAINT [FK_dbo.PersonaPOO_dbo.EstudiantePOO];

ALTER TABLE [dbo].[ProfesorPOO] WITH CHECK CHECK CONSTRAINT [FK_dbo.PersonaPOO_dbo.ProfesorPOO];


GO
PRINT N'Update complete.';


GO
