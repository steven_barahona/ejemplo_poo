﻿CREATE TABLE [dbo].[Estudiante]
(
	[Cedula] INT NOT NULL, 
    [Carnet] NVARCHAR(15) NOT NULL,
	[Carrera] INT NULL,
	[Nota] INT NULL,
	PRIMARY KEY ([Cedula]),
	CONSTRAINT [FK_dbo.Persona_dbo.Estudiante] 
	FOREIGN KEY ([Cedula]) REFERENCES [dbo].Persona 
	([Cedula])
)	