﻿CREATE TABLE [dbo].[EstudiantePOO]
(
	[Cedula] INT NOT NULL, 
    [Carnet] NVARCHAR(15) NOT NULL,
	[Carrera] INT NULL,
	[Nota] INT NULL,
	PRIMARY KEY ([Cedula]),
	CONSTRAINT [FK_dbo.PersonaPOO_dbo.EstudiantePOO] FOREIGN KEY ([Cedula]) REFERENCES [dbo].PersonaPOO ([Cedula])
)