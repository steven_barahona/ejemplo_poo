﻿CREATE TABLE [dbo].[ProfesorPOO]
(
	[Cedula] INT NOT NULL, 
    [Escuela] INT NULL,
	[Categoria] INT NULL,
	PRIMARY KEY ([Cedula]),
	CONSTRAINT [FK_dbo.PersonaPOO_dbo.ProfesorPOO] FOREIGN KEY ([Cedula]) REFERENCES [dbo].[PersonaPOO] ([Cedula])
)