﻿CREATE TABLE [dbo].[Persona]
(
	[Cedula] INT NOT NULL PRIMARY KEY, 
    [Nombre] NVARCHAR(50) NULL,
	[Apellido1] NVARCHAR(50) NULL,
	[Apellido2] NVARCHAR(50) NULL
)