﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebAppOOP.Models;
using WebAppOOP.Controllers.Reportes;

namespace WebAppOOP.Controllers
{
    public class PersonaPOOController : Controller
    {
        private ModelPOOContext db = new ModelPOOContext();
        // GET: PersonaPOO
        public ActionResult Index()
        {
            return View(db.PersonasPOO.ToList());
        }

        public ActionResult Reporte()
        {
            if(db.PersonasPOO == null)
            {
                return HttpNotFound();
            }

            return PartialView("_Reporte", db.PersonasPOO);
        }

        public ActionResult IReporte()
        {
            IReportePersonas IR = new ReportePersonasEmpty(); //ReportePersonasEmpty(); // ReportePersonas();
            return IR.Reporte(db);
        }
    }
}