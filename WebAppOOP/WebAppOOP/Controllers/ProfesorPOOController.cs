﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppOOP.Models;

namespace WebAppOOP.Controllers
{
    public class ProfesorPOOController : Controller
    {
        private ModelPOOContext db = new ModelPOOContext();
        // GET: ProfesorPOO
        public ActionResult Index()
        {
            return View(db.ProfesoresPOO.ToList());
        }
    }
}