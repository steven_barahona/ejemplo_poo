﻿using System.Web.Mvc;
using WebAppOOP.Models;

namespace WebAppOOP.Controllers.Reportes
{
    interface IReportePersonas
    {
        ActionResult Reporte(ModelPOOContext db);
    }
}