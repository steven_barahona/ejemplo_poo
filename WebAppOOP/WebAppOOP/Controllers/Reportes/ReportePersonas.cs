﻿using System.Web.Mvc;
using WebAppOOP.Models;

namespace WebAppOOP.Controllers.Reportes
{
    public class ReportePersonas : Controller, IReportePersonas
    {
        public ActionResult Reporte(ModelPOOContext db)
        {
            return PartialView("_Reporte", db.PersonasPOO);
        }
    }
}