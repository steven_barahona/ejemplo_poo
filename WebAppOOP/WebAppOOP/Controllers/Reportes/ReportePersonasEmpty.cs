﻿using System.Web.Mvc;
using WebAppOOP.Models;

namespace WebAppOOP.Controllers.Reportes
{
    public class ReportePersonasEmpty : IReportePersonas
    {
        public ActionResult Reporte(ModelPOOContext db)
        {
            return new EmptyResult();
        }
    }
}