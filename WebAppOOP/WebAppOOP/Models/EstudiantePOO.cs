﻿namespace WebAppOOP.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("EstudiantePOO")]
    public class EstudiantePOO : PersonaPOO
    {
        public string Carnet { get; set; }
        public Nullable<int> Carrera { get; set; }
        public Nullable<int> Nota { get; set; }
        public override string Descripcion
        {
            get
            {
                return "Estudiante: " + FullName + ", " + Carrera + ", " + Nota;
            }
        }
    }
}