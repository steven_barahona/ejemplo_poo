﻿namespace WebAppOOP.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("PersonaPOO")]
    public abstract class PersonaPOO
    {
        [Key]
        public int Cedula { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string FullName
        {
            get
            {
                return Nombre + ", " + Apellido1 + ", " + Apellido2;
            }
        }
        public virtual string Descripcion
        {
            get
            {
                return "Persona: " + FullName;
            }
        }

        // public abstract string Descripcion2();
    }
}