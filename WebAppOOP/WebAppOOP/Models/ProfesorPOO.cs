﻿namespace WebAppOOP.Models
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ProfesorPOO")]
    public class ProfesorPOO : PersonaPOO
    {
        public Nullable<int> Escuela { get; set; }
        public Nullable<int> Categoria { get; set; }
        public override string Descripcion
        {
            get
            {
                return "Profesor: " + FullName + ", " + Escuela + ", " + Categoria;
            }
        }
    }
}