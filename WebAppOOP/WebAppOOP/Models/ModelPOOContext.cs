﻿namespace WebAppOOP.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public class ModelPOOContext : DbContext
    {
        public ModelPOOContext()
            : base("name=DatabasePOOEntities2")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PersonaPOO>().ToTable("PersonaPOO");
            modelBuilder.Entity<ProfesorPOO>().ToTable("ProfesorPOO");
            modelBuilder.Entity<EstudiantePOO>().ToTable("EstudiantePOO");
        }

        public virtual DbSet<PersonaPOO> PersonasPOO { get; set; }
        public virtual DbSet<EstudiantePOO> EstudiantesPOO { get; set; }
        public virtual DbSet<ProfesorPOO> ProfesoresPOO { get; set; }
    }
}